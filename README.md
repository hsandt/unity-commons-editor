# Unity Commons - Editor

===

THE REPOSITORY HAS BEEN MERGED WITH OTHER UTILITY REPOSITORIES (preserving commit history) INTO THE CENTRALIZED REPOSITORY at https://github.com/hsandt/hyper-unity-commons

===

This repository contains a collection of Editor scripts for Unity I built over time. I use them for my personal projects and change them as I need, sometimes breaking compatibility. Therefore, I offer no support for them but you can see them as a collection of useful gists.

Still beware of older scripts: as long as they compile without warning, I don't update them, but some may not be relevant anymore with the newest versions of Unity.

The StagPoint and UnityToolbag folders contain partial copies of unmaintained projects, and contain their own licenses.

## Dependencies

None

## License

See [LICENSE](LICENSE) for all original scripts.

Some scripts are based on code snippets found online, sometimes with multiple contributors before myself. In this case, known contributors are listed at the beginning of the file, and the code license effective on the platform at the time the code snippet was posted applies instead (for instance, CC BY-SA 4.0 on Stack Overflow).

The StagPoint and UnityToolbag folders have their own LICENSE. StagPoint uses a copyright, but considering their tool, Immediate Window, was freely available before development stopped and it was abandoned and made unavailable, I considered it fair use to keep a copy here.
